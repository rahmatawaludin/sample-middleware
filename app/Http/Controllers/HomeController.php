<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('access-logs', ['only' => ['about']]);
    }

    public function apiTest()
    {
        return "Berhasil mengakses API";
    }

    public function logTest(Request $request)
    {
        $total_success = DB::table('access_logs')
            ->where('path', $request->path())
            ->where('status', 'success')
            ->count();
        $total_failed = DB::table('access_logs')
            ->where('path', $request->path())
            ->where('status', 'failed')
            ->count();
        return "Halaman ini telah diakses sebanyak $total_success kali sukses dan $total_failed kali gagal.";
    }

    public function adminTest()
    {
        return "Berhasil mengakses resource admin.";
    }

    public function editorTest()
    {
        return "Berhasil mengakses resource editor.";
    }

    public function showPost(Request $request, $post_id)
    {
        return $request->get('post');
    }

    public function showCustomer(Request $request, $customer_id)
    {
        return $request->get('customer');
    }

    public function about()
    {
        return "Ini tentang kita, tentang kisah kita.. #ciee..";
    }
}
