<?php

namespace App\Http\Middleware;

use Closure;

class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $access)
    {
        if ($request->has('api_token')) {
            $valid_token = \DB::table('tokens')
                ->where('key', $request->get('api_token'))
                ->where('access', $access)
                ->get();

            if ($valid_token) {
                return $next($request);
            }

            return response('API Token salah.', 403);
        }
        return response('API Token tidak diisi.', 403);
    }
}
