<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TransformResource
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $resource)
    {
        $resource_class = '\App\\' . $resource;
        $resource_id_key = $resource . '_id';
        $resource_id = $request->route()->parameters()[$resource_id_key];

        try {
            $model = $resource_class::findOrFail($resource_id);
            $request->merge([$resource => $model]);
            return $next($request);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => $resource . '_not_found'], 404);
        }
    }
}
