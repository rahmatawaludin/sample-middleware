<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use DateTime;

class AccessLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        // buat log
        DB::table('access_logs')->insert([
            'path' => $request->path(),
            'ip' => $request->getClientIp(),
            'status' => 'success',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        $request->merge(['isLogged' => 1]);

        return $response;

    }

    public function terminate($request, $response)
    {
        if (!$request->has('isLogged')) {
            // buat log
            DB::table('access_logs')->insert([
                'path' => $request->path(),
                'ip' => $request->getClientIp(),
                'status' => 'failed',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ]);
        }
    }
}
