<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('api-test', ['middleware' => 'api-token', 'uses' => 'HomeController@apiTest']);
Route::get('log-test', ['middleware' => ['api-token', 'access-log'], 'uses' => 'HomeController@logTest']);
Route::get('admin-test', ['middleware' => 'api-token:admin', 'uses' => 'HomeController@adminTest']);
Route::get('editor-test', ['middleware' => 'api-token:editor', 'uses' => 'HomeController@editorTest']);

Route::get('posts/{post_id}', ['middleware' => 'transform-resource:post', 'uses'=>'HomeController@showPost']);
Route::get('customer/{customer_id}', ['middleware' => 'transform-resource:customer', 'uses'=>'HomeController@showCustomer']);
Route::get('about', 'HomeController@about');
