<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        DB::table('tokens')->insert([
            'key' => 'indonesia-hebat',
            'access' => 'admin',
        ]);

        DB::table('tokens')->insert([
            'key' => 'bandung-juara',
            'access' => 'editor'
        ]);

        $faker = Faker\Factory::create();

        foreach (range(1,5) as $index) {
            // sample post
            $post = new \App\Post;
            $post->title = $faker->sentence(3);
            $post->content = $faker->paragraph;
            $post->save();

            // sample customer
            $gender = ['male', 'female'][rand(0,1)];
            $customer = new \App\Customer;
            $customer->name = $faker->name($gender);
            $customer->gender = $gender;
            $customer->address = $faker->address;
            $customer->save();
        }


        Model::reguard();
    }
}
